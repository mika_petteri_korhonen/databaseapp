CREATE TABLE `field_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `databaseName` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `tableName` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `fieldName` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `regex` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
