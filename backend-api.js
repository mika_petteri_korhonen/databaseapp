
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
// Database
var mysql      = require('mysql');
var mysqlConfig = require("./mysql.config");
var connection = mysql.createConnection({
  host     : mysqlConfig.config.host,
  user     : mysqlConfig.config.user,
  password : mysqlConfig.config.password,
  database : mysqlConfig.config.database
});
connection.connect(function(err){
    if(err){
        console.error(err);
    }else{
        console.log('connected');
    }
});

var app = express();
var jsonParser = bodyParser.json();
app.use(bodyParser.urlencoded({
  extended: true
})); 
app.use(cors());

app.post("/databases/create", function(req, res) {
    var query = 'CREATE SCHEMA `' + connection.escape(req.query.databaseName).replace(/'/g, "") + '` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});
app.post("/databases/drop", function(req, res) {
    var query = 'DROP SCHEMA `' + connection.escape(req.query.databaseName).replace(/'/g, "") + '`;';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});
app.get("/databases", function(req, res) {
    var query = 'SHOW databases;';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.post("/tables/createTable", function(req, res) {
    var query = 'CREATE TABLE `' + connection.escape(req.query.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.query.tableName).replace(/'/g, "") + '` ('+
                '`id` INT NOT NULL AUTO_INCREMENT,'+
                'PRIMARY KEY (`id`));';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.get("/tables/show/:databaseName", function(req, res) {
    var query = 'SHOW tables FROM `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`;';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});
app.get("/tables/description/:databaseName/:tableName", function(req, res) {
    var query = 'SHOW COLUMNS FROM `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.params.tableName).replace(/'/g, "") + '`;';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});
app.get("/tables/create/:databaseName/:tableName", function(req, res) {
    var query = 'SHOW CREATE TABLE `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.params.tableName).replace(/'/g, "") + '`;';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.get("/tables/index/:databaseName/:tableName", function(req, res) {
    var query = 'SHOW INDEX FROM `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.params.tableName).replace(/'/g, "") + '`;';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});
app.get("/tables/addIndex/:databaseName/:tableName/:fieldName", function(req, res) {
    var query = 'ALTER TABLE `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.params.tableName).replace(/'/g, "") + '` ADD INDEX `' + connection.escape(req.params.fieldName).replace(/'/g, "") + '` (`' + connection.escape(req.params.fieldName).replace(/'/g, "") + '`);';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.get("/tables/forreignKeys/:databaseName/:tableName/:columnName", function(req, res) {
    var query = 'SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_NAME = "' + connection.escape(req.params.tableName).replace(/'/g, "") + '" AND REFERENCED_COLUMN_NAME = "' + connection.escape(req.params.columnName).replace(/'/g, "") + '" AND TABLE_SCHEMA = "' + connection.escape(req.params.databaseName).replace(/'/g, "") + '";';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.get("/tables/AlterTable/:databaseName/:tableName/:columName", function(req, res) {
    var query = 'ALTER TABLE `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.params.tableName).replace(/'/g, "") + '` ADD `' + connection.escape(req.params.columName).replace(/'/g, "") + '` VARCHAR(255);';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.get("/tables/:databaseName/:tableName/:sortBy/:sortOrder", function(req, res) {
    var query = 'SELECT * FROM `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.params.tableName).replace(/'/g, "") + '` ' +
    ' ORDER BY ' + connection.escape(req.params.sortBy).replace(/'/g, "") + 
    ' ' + connection.escape(req.params.sortOrder).replace(/'/g, "") + ';';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.post("/tables/:databaseName/:tableName/", function(req, res) {
    console.log(req.query);
    var columnNames = Object.getOwnPropertyNames(req.query);
    var updatedColums = [];
    var updatedColumnsVals = [];
    for(var i = 0; i < columnNames.length; i++) {
        if(columnNames[i] != "id") {
            updatedColums.push(columnNames[i]);
            updatedColumnsVals.push(connection.escape(req.query[columnNames[i]]));
        }
    }
    var query = 'INSERT INTO `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.params.tableName).replace(/'/g, "") + '` (' + updatedColums.join(",") + ') VALUES(' + updatedColumnsVals.join(",") + ');';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.put("/tables/:databaseName/:tableName/", function(req, res) {
    console.log(req.query);
    var columnNames = Object.getOwnPropertyNames(req.query);
    var updatedColumnsVals = [];
    for(var i = 0; i < columnNames.length; i++) {
        if(columnNames[i] != "id") {
            updatedColumnsVals.push(columnNames[i] + " = " + connection.escape(req.query[columnNames[i]]));
        }
    }
    var query = 'UPDATE `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.params.tableName).replace(/'/g, "") + '` SET ' + updatedColumnsVals.join(",") + ' WHERE id = ' + connection.escape(req.query.id) + ';';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.delete("/tables/:databaseName/:tableName/", jsonParser, function(req, res) {
    var query = 'DELETE FROM `' + connection.escape(req.params.databaseName).replace(/'/g, "") + '`.`' + connection.escape(req.params.tableName).replace(/'/g, "") + '` WHERE customerId = ' + connection.escape(req.query.customerId).replace(/'/g, "") + ';';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.get("/tables/FieldProperties/:databaseName/:tableName", function(req, res) {
    var query = 'SELECT * FROM `databaseApp`.`field_properties` WHERE databaseName=' + connection.escape(req.params.databaseName) + ' AND tableName=' + connection.escape(req.params.tableName) + ';';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.post("/tables/FieldProperties/", function(req, res) {
    console.log(req.query);
    var columnNames = Object.getOwnPropertyNames(req.query);
    var updatedColums = [];
    var updatedColumnsVals = [];
    for(var i = 0; i < columnNames.length; i++) {
        if(columnNames[i] != "id") {
            updatedColums.push(columnNames[i]);
            updatedColumnsVals.push(connection.escape(req.query[columnNames[i]]));
        }
    }
    var query = 'INSERT INTO `databaseApp`.`field_properties` (' + updatedColums.join(",") + ') VALUES(' + updatedColumnsVals.join(",") + ');';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});

app.put("/tables/FieldProperties/", function(req, res) {
    console.log(req.query);
    var columnNames = Object.getOwnPropertyNames(req.query);
    var updatedColumnsVals = [];
    for(var i = 0; i < columnNames.length; i++) {
        if(columnNames[i] != "id") {
            updatedColumnsVals.push(columnNames[i] + " = " + connection.escape(req.query[columnNames[i]]));
        }
    }
    var query = 'UPDATE `databaseApp`.`field_properties` SET ' + updatedColumnsVals.join(",") + ' WHERE id = ' + connection.escape(req.query.id) + ';';
    console.log(query);
    connection.query(query, function (error, results, fields) {
        if (error) console.log(error);
        res.send({
            results
        });
    });
});


const port = 3000;
const ip = 'localhost';
if (!module.parent) {
    app.listen(port, ip);
    console.log('Express started on port: '+ port + ' and ip: ' + ip);
}