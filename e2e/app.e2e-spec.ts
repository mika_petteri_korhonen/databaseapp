import { InlatisPreworkPage } from './app.po';

describe('inlatis-prework App', () => {
  let page: InlatisPreworkPage;

  beforeEach(() => {
    page = new InlatisPreworkPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
