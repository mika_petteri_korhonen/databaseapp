CREATE TABLE `customers` (
  `customerId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_bin NOT NULL,
  `address` varchar(45) CHARACTER SET utf8 NOT NULL,
  `postNbr` varchar(45) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`customerId`,`postNbr`,`address`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
