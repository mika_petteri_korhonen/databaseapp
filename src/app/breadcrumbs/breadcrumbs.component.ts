import { Component, OnInit } from '@angular/core';
import { BreadcrumbsService } from '../services/breadcrumbs.service';
import { DatabasesService } from '../services/databases.service';
@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  private breadCrumbs = [];
  constructor(private bc: BreadcrumbsService,
              private db: DatabasesService) {
    this.bc.breadcrumbsObservable.subscribe(response => {
      if(response != null) {
        this.breadCrumbs = response;
      }
    });
  }

  ngOnInit() {
  }
  public breadCrumbEvent(event, url, title) {
    if(url === "/" && title === "Etusivu") {
      this.bc.cearCrumbs();
      this.db.selecteDB.next("");
    }
  }
}
