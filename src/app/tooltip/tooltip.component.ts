import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnInit {
  @Input() tooltipBody = "";
  @Input() tooltipHeader = "";
  @Input() tooltipValue = "";
  @Output() callbackEvent: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  public confirm(event) {
    this.callbackEvent.next({
      answer: true,
      value: this.tooltipValue
    });
  }
  public deny(event) {
    this.callbackEvent.next({
      answer: false,
      value: this.tooltipValue
    });
  }
}
