import { Component, OnInit, Input } from '@angular/core';
import { TablesService } from '../services/tables.service';

@Component({
  selector: 'app-tables-table-fields',
  templateUrl: './tables-table-fields.component.html',
  styleUrls: ['./tables-table-fields.component.scss']
})
export class TablesTableFieldsComponent implements OnInit {
  @Input() tableName = "";
  @Input() tableDescriptions = [];
  constructor(private tables: TablesService) {
    this.tables.tableForreignKeysObservable.subscribe(response => {
      if(response != null) {
        console.log(response);
        //this.tableForreignKeys = response.results;
      }
    });
  }

  ngOnInit() {
  }
  public showForreignKeys(event, tableName, columnName) {
    event.preventDefault();
    this.tables.fetchTableForreignKeys(tableName, columnName);
  }
  public addIndex(event, tableName, columnName) {
    event.preventDefault();
    this.tables.addTableIndex(tableName, columnName);
    this.tables.fetchTableDescription(this.tableName);
  }
}
