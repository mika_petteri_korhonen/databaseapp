import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesTableFieldsComponent } from './tables-table-fields.component';

describe('TablesTableFieldsComponent', () => {
  let component: TablesTableFieldsComponent;
  let fixture: ComponentFixture<TablesTableFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablesTableFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesTableFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
