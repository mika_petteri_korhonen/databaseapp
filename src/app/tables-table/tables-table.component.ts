import { Component, OnInit, Input } from '@angular/core';
import { TablesService } from '../services/tables.service';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-tables-table',
  templateUrl: './tables-table.component.html',
  styleUrls: ['./tables-table.component.scss'],
  providers: [ TablesService, CustomerService ]
})
export class TablesTableComponent implements OnInit {
  private tableDescriptions = [];
  private editTable = false;
  private editTableFieldsBool = false;
  private showTableCreate = false;
  private showTableIndex = false;
  private tableCreation = "";
  private tableIndex = [];
  private tableForreignKeys = [];
  @Input() tableName = "";
  constructor(private tables: TablesService, private customer: CustomerService) {
    this.tables.tableDescriptionObservable.subscribe(response => {
      if(response != null) {
        this.customer.sortColumn = response.results[0].Field;
        if(this.tableDescriptions.length > 0) {
          this.tableDescriptions = [];
        }
        for(var i = 0; i < response.results.length; i++) {
          this.tableDescriptions.push(response.results[i]);
        }
      }
    });
    this.tables.tableCreationObservable.subscribe(response => {
      if(response != null) {
        this.tableCreation = response.results[0]['Create Table'];
      }
    });
    this.tables.tableIndexObservable.subscribe(response => {
      if(response != null) {
        this.tableIndex = response.results;
      }
    });
  }

  ngOnInit() {
    this.tables.fetchTableDescription(this.tableName);
  }
  public editTableContent(event) {
    event.preventDefault();
    this.editTable = !this.editTable;
    if(this.editTableFieldsBool) {
      this.editTableFieldsBool = false;
    }
    if(this.showTableCreate) {
      this.showTableCreate = false;
    }
    if(this.showTableIndex) {
      this.showTableIndex = false;
    }
  }
  public editTableFields(event) {
    event.preventDefault();
    this.editTableFieldsBool = !this.editTableFieldsBool;
    this.tables.fetchTableDescription(this.tableName);
    if(this.editTable) {
      this.editTable = false;
    }
    if(this.showTableCreate) {
      this.showTableCreate = false;
    }
    if(this.showTableIndex) {
      this.showTableIndex = false;
    }
  }
  public showCreate(event, tableName) {
    event.preventDefault();
    this.tables.fetchTableCreation(tableName);
    this.showTableCreate = !this.showTableCreate;
    if(this.editTable) {
      this.editTable = false;
    }
    if(this.editTableFieldsBool) {
      this.editTableFieldsBool = false;
    }
    if(this.showTableIndex) {
      this.showTableIndex = false;
    }
  }
  public showIndex(event, tableName) {
    event.preventDefault();
    this.tables.fetchTableIndex(tableName);
    this.showTableIndex = !this.showTableIndex;
    if(this.editTable) {
      this.editTable = false;
    }
    if(this.editTableFieldsBool) {
      this.editTableFieldsBool = false;
    }
    if(this.showTableCreate) {
      this.showTableCreate = false;
    }
  }
}
