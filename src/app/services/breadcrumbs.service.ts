import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class BreadcrumbsService {
  public breadcrumbsArr = [];
  public breadcrumbs = new BehaviorSubject(null);
  public breadcrumbsObservable = this.breadcrumbs.asObservable();
  constructor() {
    this.breadcrumbsArr.push({
      title: "Etusivu",
      url: "/"
    });
    this.breadcrumbs.next(this.breadcrumbsArr);
  }

  public addToBreadCrumbs(title, url) {
    if(!this.crumbFound(title, url)) {
      this.breadcrumbsArr.push({
        title: title,
        url: url
      });
      this.breadcrumbs.next(this.breadcrumbsArr);
    }
  }
  public cearCrumbs() {
    this.breadcrumbsArr= [];
    this.breadcrumbsArr.push({
      title: "Etusivu",
      url: "/"
    });
    this.breadcrumbs.next(this.breadcrumbsArr);
  }
  public crumbFound(title, url) {
    for(var i = 0; i < this.breadcrumbsArr.length; i++) {
      if(this.breadcrumbsArr[i].title === title && this.breadcrumbsArr[i].url === url) {
        return true;
      }
    }
    return false;
  }
}
