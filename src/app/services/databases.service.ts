import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class DatabasesService {
  public apiLocation = "http://localhost:3000";

  public selecteDB = new BehaviorSubject(null);
  public selecteDBObservable = this.selecteDB.asObservable();

  public databases = new BehaviorSubject(null);
  public databasesObservable = this.databases.asObservable();
  public databasesUrl = this.apiLocation + "/databases";
  public createDatabaseUrl = this.apiLocation + "/databases/create";
  public dropDatabaseUrl = this.apiLocation + "/databases/drop";

  constructor(private http: Http) { }

  public fetchDatabases():void {
    this.http.get(this.databasesUrl)
    .map(response => response.json())
    .subscribe((data) => {
      this.databases.next(data);
    });
  }
  public createDatabase(databaseName):void {
    this.http.post(this.createDatabaseUrl + "/?databaseName=" + databaseName, [])
    .map(response => response.json())
    .subscribe((data) => {
      this.fetchDatabases();
    });
  }
  public dropDatabase(databaseName):void {
    this.http.post(this.dropDatabaseUrl + "/?databaseName=" + databaseName, [])
    .map(response => response.json())
    .subscribe((data) => {
      this.fetchDatabases();
    });
  }
}
