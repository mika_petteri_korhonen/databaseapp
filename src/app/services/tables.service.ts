import { Injectable } from '@angular/core';
import { DatabasesService } from './databases.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class TablesService {
  public apiLocation = "http://localhost:3000";
  
  public tables = new BehaviorSubject(null);
  public tablesObservable = this.tables.asObservable();
  public tablesUrl = this.apiLocation + "/tables";
  
  public tableDescription = new BehaviorSubject(null);
  public tableDescriptionObservable = this.tableDescription.asObservable();
  public tableDescriptionUrl = this.apiLocation + "/tables/description";
  
  public tableCreation = new BehaviorSubject(null);
  public tableCreationObservable = this.tableCreation.asObservable();
  public tableCreationUrl = this.apiLocation + "/tables/create";
    
  public tableIndex = new BehaviorSubject(null);
  public tableIndexObservable = this.tableIndex.asObservable();
  public tableIndexUrl = this.apiLocation + "/tables/index";

  public tableForreignKeys = new BehaviorSubject(null);
  public tableForreignKeysObservable = this.tableIndex.asObservable();
  public tableForreignKeysUrl = this.apiLocation + "/tables/forreignKeys";

  public tableAddIndex = new BehaviorSubject(null);
  public tableAddIndexObservable = this.tableAddIndex.asObservable();
  public tableAddIndexUrl = this.apiLocation + "/tables/addIndex";

  public createTableUrl = this.apiLocation + "/tables/createTable";

  private selectedDB = "";
  constructor(private http: Http, private db: DatabasesService) {
    this.db.selecteDBObservable.subscribe(response => {
      if(response != null) {
        this.selectedDB = response;
      }
    });
  }

  public fetchTables():void {
    this.http.get(this.tablesUrl + "/show/" + this.selectedDB)
    .map(response => response.json())
    .subscribe((data) => {
      this.tables.next(data);
    });
  }

  public fetchTableDescription(tableName):void {
    this.http.get(this.tableDescriptionUrl + "/" + this.selectedDB + "/" + tableName)
    .map(response => response.json())
    .subscribe((data) => {
      this.tableDescription.next(data);
    });
  }
  public fetchTableCreation(tableName):void {
    this.http.get(this.tableCreationUrl + "/" + this.selectedDB + "/" + tableName)
    .map(response => response.json())
    .subscribe((data) => {
      this.tableCreation.next(data);
    });
  }
  public fetchTableIndex(tableName):void {
    this.http.get(this.tableIndexUrl + "/" + this.selectedDB + "/" + tableName)
    .map(response => response.json())
    .subscribe((data) => {
      this.tableIndex.next(data);
    });
  }
  public fetchTableForreignKeys(tableName, columnName):void {
    this.http.get(this.tableForreignKeysUrl + "/" + this.selectedDB + "/" + tableName + "/" + columnName)
    .map(response => response.json())
    .subscribe((data) => {
      this.tableForreignKeys.next(data);
    });
  }
  public addTableIndex(tableName, columnName):void {
    this.http.get(this.tableAddIndexUrl + "/" + this.selectedDB + "/" + tableName + "/" + columnName)
    .map(response => response.json())
    .subscribe((data) => {
      this.tableAddIndex.next(data);
    });
  }
  public createTable(tableName):void {
    this.http.post(this.createTableUrl + "/?databaseName=" + this.selectedDB + "&tableName=" + tableName, [])
    .map(response => response.json())
    .subscribe((data) => {
      console.log(data);
      this.fetchTables();
    });
  }
}
