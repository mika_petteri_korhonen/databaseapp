import { Injectable } from '@angular/core';
import { TablesService } from './tables.service';
import { DatabasesService } from './databases.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class CustomerService {
  public apiLocation = "http://localhost:3000";
  public sortColumn = "id";
  public sortOrderArr = ["DESC", "ASC"];
  public sortOrder = this.sortOrderArr[1];
  public columnNames = [];
  public customers = new BehaviorSubject(null);
  public customersObservable = this.customers.asObservable();
  public fieldPropertie = new BehaviorSubject(null);
  public fieldPropertieObservable = this.fieldPropertie.asObservable();
  public tableName = "customers";
  public customersUrl = this.apiLocation + "/tables";
  private selectedDB = "";
  constructor(private http: Http,
              private tables: TablesService,
              private db: DatabasesService) {
    this.tables.tableDescriptionObservable.subscribe(response => {
      if(response != null) {
        this.sortColumn = response.results[0].Field;
      }
    });
    this.db.selecteDBObservable.subscribe(response => {
      if(response != null) {
        this.selectedDB = response;
      }
    });
  }

  public fetchTable(tableName):void {
    this.tableName = tableName;
    this.http.get(this.customersUrl + "/" + this.selectedDB + "/" + this.tableName + "/" + this.sortColumn + "/" + this.sortOrder)
    .map(response => response.json())
    .subscribe((data) => {
      this.customers.next(data);
    });
  }
  public getUrl(customer):string {
    let url = this.customersUrl + "/" + this.selectedDB + "/" + this.tableName + "/?";
    for(let i = 0; i < this.columnNames.length; i++) {
      if(i === (this.columnNames.length - 1)) {
        url += this.columnNames[i] + "=" + customer[this.columnNames[i]];
      } else {
        url += this.columnNames[i] + "=" + customer[this.columnNames[i]] + "&";
      }
    }
    return url;
  } 
  public insertCustomer(customer):void {
    this.http.post(this.getUrl(customer), [customer] )
    .map(response => response.json())
    .subscribe((data) => {
      this.fetchTable(this.tableName);
    });
  }
  public updateCustomer(customer):void {
    this.http.put(this.getUrl(customer), [customer])
    .map(response => response.json())
    .subscribe((data) => {
      this.fetchTable(this.tableName);
    });
  }
  public deleteCustomer(customer):void {
    this.http.delete(this.customersUrl + "/" + this.selectedDB + "/?customerId=" + customer.customerId)
    .map(response => response.json())
    .subscribe((data) => {
      this.fetchTable(this.tableName);
    });
  }
  public alterTable(tableName, columnName):void {
    this.http.get(this.customersUrl + "/AlterTable/" + this.selectedDB + "/" + tableName + "/" + columnName)
    .map(response => response.json())
    .subscribe((data) => {
      this.fetchTable(this.tableName);
      this.tables.fetchTableDescription(this.tableName);
    });
  }
  public fieldProperties():void {
    this.http.get(this.customersUrl + "/FieldProperties/" + this.selectedDB + "/" + this.tableName)
    .map(response => response.json())
    .subscribe((data) => {
      this.fieldPropertie.next(data);
    });
  }
  public saveFieldProperties(field, regex):void {
    this.http.post(this.customersUrl + "/FieldProperties/?databaseName=" +  this.selectedDB+ "&tableName=" + this.tableName + "&fieldName=" + field + "&regex=" + regex, [])
    .map(response => response.json())
    .subscribe((data) => {
      this.fieldProperties();
    });
  }
  public updateFieldProperties(id, field, regex):void {
    this.http.put(this.customersUrl + "/FieldProperties/?databaseName=" +  this.selectedDB+ "&tableName=" + this.tableName + "&id=" + id + "&fieldName=" + field + "&regex=" + regex, [])
    .map(response => response.json())
    .subscribe((data) => {
      this.fieldProperties();
    });
  }
}
