import { Component, OnInit } from '@angular/core';
import { DatabasesService } from '../services/databases.service';
import { BreadcrumbsService } from '../services/breadcrumbs.service';

@Component({
  selector: 'app-databases',
  templateUrl: './databases.component.html',
  styleUrls: ['./databases.component.scss']
})
export class DatabasesComponent implements OnInit {
  private databases = [];
  private selectedDB = "";
  private tooltipBody = "";
  private tooltipHeader = "";
  private tooltipValue = "";
  private showTooltip = false;
  constructor(private db: DatabasesService,
              private bc: BreadcrumbsService) {
    this.db.databasesObservable.subscribe(response => {
      if(response != null) {
        if(this.databases.length > 0) {
          this.databases = [];
        }
        for(var i = 0; i < response.results.length; i++) {
          this.databases.push(response.results[i]);
        }
      }
    });
    this.db.selecteDBObservable.subscribe(response => {
      if(response != null) {
        this.selectedDB = response;
      }
    });
  }

  ngOnInit() {
    this.db.fetchDatabases();
  }
  public selectDatabase(event, database) {
    this.bc.addToBreadCrumbs(database, "/database/" + database);
    this.db.selecteDB.next(database);
  }
  public createNewDatabase(event, databaseName) {
    this.db.createDatabase(databaseName.innerText);
  }
  public makeEditable(event) {
    event.preventDefault();
    event.srcElement.contentEditable = true;
    event.srcElement.focus();
  }
  public dropDatabase(event, databaseName):void {
    event.preventDefault();
    this.showTooltip = true;
    this.tooltipValue = databaseName;
    this.tooltipBody = "Tämä toiminto poistaa tietokannan " + databaseName + "!";
    this.tooltipHeader = "Tietokannan poisto";
  }
  public tooltipCallbackEvent(event):void {
    this.showTooltip = false;
    if(event.answer) {
      this.db.dropDatabase(event.value);
    }
  }
  public allowedDatabases(databaseName) {
    let retVal = true;
    switch(databaseName) {
      case "sys":
      case "mysql":
      case "information_schema":
      case "performance_schema":
      case "databaseApp":
        retVal = false;
        break;
      default:
        retVal = true;
        break;
    }
    return retVal;
  }
}
