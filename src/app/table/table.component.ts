import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { CustomerService } from '../services/customer.service';
import { TablesService } from '../services/tables.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  public customers = [];
  public columnNames = [];
  private columnProperties = [];
  @Input() tableName = "";
  constructor(private customer: CustomerService, 
              private elmref: ElementRef,
              private tables: TablesService) {
    this.tables.tableDescriptionObservable.subscribe(response => {
      if(response != null) {
        if(this.customer.columnNames.length > 0) {
          this.customer.columnNames = [];
        }
        for(let i = 0; i < response.results.length; i++) {
            this.customer.columnNames.push(response.results[i].Field);
        }
        this.columnNames = this.customer.columnNames;
      }
    });
    this.customer.customersObservable.subscribe(response => {
      if(response != null) {
        if(response.results.length > 0) {
          this.customers = response.results;
        }
      }
    });
    this.customer.fieldPropertieObservable.subscribe(response => {
      if(response != null) {
        this.columnProperties = response.results;
      }
    });
  }

  ngOnInit() {
    this.customer.fetchTable(this.tableName);
    this.tables.fetchTableDescription(this.tableName);
    this.customer.fieldProperties();
  }
  public checkRegex(event, field):void {
    for(var i=0; i < this.columnProperties.length; i++) {
      if(this.columnProperties[i].fieldName === field) {
        let text = event.srcElement.value;
        let regExp = new RegExp(this.columnProperties[i].regex);
        if(regExp.test(text)) {
          event.srcElement.className = "pass";
        } else {
          event.srcElement.className = "fail";
        }
      }
    }
  }
  public addCustomer(event) {
    event.preventDefault();
    let customerObject = {};
    for(var i = 0; i < this.columnNames.length; i++) {
      if(i > 0) {
        var elem = this.elmref.nativeElement.querySelector("input#" +this.columnNames[i]);
        var tempElem = elem.value;
        elem.value = "";
        customerObject[this.columnNames[i]] = tempElem;
      }
    }
    this.customer.insertCustomer(customerObject);
  }
  public flipOrder() {
    if(this.customer.sortOrder === "ASC") {
      this.customer.sortOrder = this.customer.sortOrderArr[0];
    } else {
      this.customer.sortOrder = this.customer.sortOrderArr[1];
    }
  }
  public removeCustomer(event, customerObj) {
    event.preventDefault();
    this.customer.deleteCustomer(customerObj);
  }
  public makeEditable(event) {
    event.srcElement.contentEditable = true;
    event.srcElement.focus();
  }
  public saveCell(event, customerObj, field) {
    event.srcElement.contentEditable = false;
    customerObj[field] = event.srcElement.innerHTML;
    this.customer.updateCustomer(customerObj);
  }
  public sortBy(event, column) {
    this.customer.sortColumn = column;
    this.flipOrder();
    this.customer.fetchTable(this.tableName);
  }
  public alterColumn(event, columnName):void {
    this.customer.alterTable(this.tableName, columnName);
  }

}
