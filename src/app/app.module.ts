import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { CustomerService } from './services/customer.service';
import { TablesService } from './services/tables.service';
import { DatabasesService } from './services/databases.service';
import { BreadcrumbsService } from './services/breadcrumbs.service';


import { AppComponent } from './app.component';
import { FieldPropertiesComponent } from './field-properties/field-properties.component';
import { TableComponent } from './table/table.component';
import { TablesComponent } from './tables/tables.component';
import { TablesTableComponent } from './tables-table/tables-table.component';
import { TablesTableIndexesComponent } from './tables-table-indexes/tables-table-indexes.component';
import { TablesTableFieldsComponent } from './tables-table-fields/tables-table-fields.component';
import { DatabasesComponent } from './databases/databases.component';
import { TooltipComponent } from './tooltip/tooltip.component';
import { FourOfourComponent } from './four-ofour/four-ofour.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';



const reitit = [
  { path: "", component: DatabasesComponent },
  { path: "database/:database", component: TablesComponent },
  { path: "**", component: FourOfourComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    FieldPropertiesComponent,
    TableComponent,
    TablesComponent,
    TablesTableComponent,
    TablesTableIndexesComponent,
    TablesTableFieldsComponent,
    DatabasesComponent,
    TooltipComponent,
    FourOfourComponent,
    BreadcrumbsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(reitit)
  ],
  providers: [ 
    TablesService,
    DatabasesService,
    BreadcrumbsService
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
