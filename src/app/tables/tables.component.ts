import { Component, OnInit, Input } from '@angular/core';
import { TablesService } from '../services/tables.service';
import { DatabasesService } from '../services/databases.service';
import { ActivatedRoute } from '@angular/router';
import { BreadcrumbsService } from '../services/breadcrumbs.service';
@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {
  private foundTables = [];
  private database = "";
  constructor(private tables: TablesService,
              private db: DatabasesService,
              private route: ActivatedRoute,
              private bc: BreadcrumbsService) {
    this.tables.tablesObservable.subscribe(response => {
      if(response != null) {
        if(response.results.length > 0) {
          let param = Object.getOwnPropertyNames(response.results[0]);
          if(this.foundTables.length > 0) {
            this.foundTables = [];
          }
          for(var i = 0; i < response.results.length; i++) {
            this.foundTables.push(response.results[i][param[0]]);
          }
        }
      }
    });
    this.db.selecteDBObservable.subscribe(response => {
      if(response != null) {
        console.log(response);
        this.database = response;
        this.tables.fetchTables();
      }
    });
    this.route.params.subscribe(params => {
      this.database = params['database'];
      this.db.selecteDB.next(params['database']);
      this.bc.addToBreadCrumbs(this.database, "/database/" + this.database);
      this.tables.fetchTables();
    });
  }

  ngOnInit() {
  }
  public createNewTable(event, tableName) {
    this.tables.createTable(tableName.innerText);
  }
  public makeEditable(event) {
    event.srcElement.contentEditable = true;
    event.srcElement.focus();
  }
}
