import { Component, OnInit, Input } from '@angular/core';
import { CustomerService } from '../services/customer.service';
@Component({
  selector: 'app-field-properties',
  templateUrl: './field-properties.component.html',
  styleUrls: ['./field-properties.component.scss']
})
export class FieldPropertiesComponent implements OnInit {
  public visible = false;
  public value = "";
  private found = false;
  private id = 0;
  @Input() fieldName = "";
  constructor(private customer: CustomerService) {
    this.customer.fieldPropertieObservable.subscribe(response => {
      if(response != null) {
        for(var i = 0; i < response.results.length; i++) {
          if(response.results[i].fieldName == this.fieldName) {
            this.value = response.results[i].regex;
            this.found = true;
            this.id = response.results[i].id;
          }
        }
      }
    });
  }

  ngOnInit() {
  }
  public showConfigs():void {
    this.visible = !this.visible;
  }
  public makeEditable(event):void {
    event.srcElement.contentEditable = true;
    event.srcElement.focus();
  }
  public saveCell(event):void {
    this.showConfigs();
    let value = event.srcElement.innerText;
    if(this.found) {
      this.customer.updateFieldProperties(this.id, this.fieldName, value);
    } else {
      this.customer.saveFieldProperties(this.fieldName, value);
    }
  }
}
