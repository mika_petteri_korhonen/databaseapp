import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesTableIndexesComponent } from './tables-table-indexes.component';

describe('TablesTableIndexesComponent', () => {
  let component: TablesTableIndexesComponent;
  let fixture: ComponentFixture<TablesTableIndexesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablesTableIndexesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesTableIndexesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
