import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tables-table-indexes',
  templateUrl: './tables-table-indexes.component.html',
  styleUrls: ['./tables-table-indexes.component.scss']
})
export class TablesTableIndexesComponent implements OnInit {
  @Input() tableIndex = [];
  constructor() { }

  ngOnInit() {
  }

}
